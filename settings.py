from django.conf import settings

# 注册app
app = 'plugins.plugins_market_backend'
if app not in settings.INSTALLED_APPS:
    settings.INSTALLED_APPS += [app]
